#!/usr/bin/env python
# encoding: utf-8

"""
update from rss, post the content to Sina Weibo
"""
# Copyright (C) by 
# Chu Yanshuo <chu@yanshuo.name>
# All rights reserved


import feedparser
import SinaWeibo
import re

class WebSiteRss(object):

    """Docstring for WebSiteRss. """

    def __init__(self,rss):
        """@todo: to be defined1.

        :rss: @todo

        """
        self._rss = rss
        try:
            self.parser=feedparser.parse(self._rss)
        except:
            print "rss address is down"

    def getLatestID(self):
        return self.parser.entries[0].id

    def getLatestContent(self):
        summary=self.parser.entries[0].summary
        TAG_RE = re.compile(r'<[^>]+>')
        summary=TAG_RE.sub("",summary)
        summary=summary.replace("\n", "/")
        summary=summary[0:100]
        return summary

    def getLatestTitle(self):
        return self.parser.entries[0].title

    def getLatestAuthor(self):
        return self.parser.entries[0].author


    def __eq__(self, other):
        this=self.parser.entries[0].published_parsed
        that=other.parser.entries[0].published_parsed
        return this==that
    
    def __ne__(self, other):
        this=self.parser.entries[0].published_parsed
        that=other.parser.entries[0].published_parsed
        return not this==that
        
    
    def __lt__(self, other):
        this=self.parser.entries[0].published_parsed
        that=other.parser.entries[0].published_parsed
        return this<that
    
    def __le__(self, other):
        this=self.parser.entries[0].published_parsed
        that=other.parser.entries[0].published_parsed
        return this<that or this==that
    
    def __gt__(self, other):
        this=self.parser.entries[0].published_parsed
        that=other.parser.entries[0].published_parsed
        return this>that
    
    def __ge__(self, other):
        this=self.parser.entries[0].published_parsed
        that=other.parser.entries[0].published_parsed
        return this>that or this == that
    
        pass


def isRSSUpdated(recentPost):
    """
    :returns: @todo

    """
    try:
        logFile=open("log.txt")
        lastID=logFile.readline().strip()
    except IOError:
        lastID=""

    latestID=recentPost.getLatestID()
    if not latestID==lastID:
        print "RSS has been updated"
        with open("log.txt",'w') as logFile:
            logFile.write(latestID)
        return True

    return False


def main():
    """
    :returns: @todo

    """

    cn=WebSiteRss("http://yanshuo.name/cn/feed/")
    en=WebSiteRss("http://yanshuo.name/en/feed/")
    if cn > en:
        recentPost=cn
    else:
        recentPost=en

    if isRSSUpdated(recentPost):
        content=u'发布了博文：《'+recentPost.getLatestTitle()+u"》"+recentPost.getLatestContent()+u"……链接地址："+recentPost.getLatestID().decode("UTF-8")
        wb=SinaWeibo.SinaWeibo()
        wb.post(_status=content)
    else:
        print "RSS does not update currently."

    pass

if __name__ == '__main__':
    main()
